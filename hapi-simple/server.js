'use strict';
 const Hapi = require('hapi');
 const server = new Hapi.Server();

 server.connection({port:3000,host:'localhost'});

 server.route({
     method:'GET',
     path:'/',
     handler: function (request, reply) {
         reply("Its works");
     }
 });

server.route({
    method:'GET',
    path:'/page/{param}',
    handler: function (request, reply) {
        reply("Page " + request.params.param);
    }
})


 server.start(err=>{
    if(err){
        throw err;
    }
    console.log('Server running at: ', server.info.uri);
 });